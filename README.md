# Nmap
Nmap (“Network Mapper”) is an open source tool for network exploration and security auditing. Nmap uses raw IP packets in novel ways to determine what hosts are available on the network, what services (application name and version) those hosts are offering, what type of packet filters/firewalls are in use, and other characteristics.
