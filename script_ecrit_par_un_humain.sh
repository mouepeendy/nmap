#!/bin/bash


echo "Enter the first input:"
read read1 < /dev/stdin

echo "Enter the second input:"
read read2 < /dev/stdin


output=$(eval $read1)
output2=$(eval $read2 && cat result.txt)
# Utilise awk pour extraire les lignes contenant les résultats de numérisation des ports
port_lines=$(echo "$output" | awk '/^([0-9]+\/)/')
port_lines2=$(echo "$output2" | awk '/^([0-9]+\/)/')
# Compte les occurrences des mots "open", "filtered" et "closed" dans les lignes extraites
open_count=$(echo "$port_lines" | grep -c "open")
open_count2=$(echo "$output2" | grep -c "Open")
filtered_count=$(echo "$port_lines" | grep -c "filtered")
filtered_count2=$(echo "$output2" | grep -c "Filtered")
closed_count=$(echo "$port_lines" | grep -c "closed")
closed_count2=$(echo "$output2" | grep -c "Closed")


# Recherche les lignes contenant "not shown" et récupère le nombre de ports correspondants
not_shown_nbr=$(echo "$output" | awk '/Not shown:/' | cut -d " " -f 3)
#not_shown_nbr=$(echo "$output" | awk '/not shown:/' | cut -d " " -f 3)
type_status=$(echo "$output" | awk '/Not shown:/' | cut -d " " -f 4)
echo $not_shown 
if [[ "$type_status" == "closed" ]];then
	closed_count=$((closed_count + not_shown_nbr))
elif [[ "$type_status" == "open" ]];then
	open_count=$((open_count + not_shown_nbr))
elif [[ "$type_status" == "filtered" ]];then
	filtered_count=$((filtered_count + not_shown_nbr))
fi
# Ajoute les ports "not shown" aux compteurs correspondants (open ou closed)
# Affiche les résultats
echo "nmap: Nombre de ports ouverts : $open_count"
echo "./ft_nmap: Nombre de ports ouverts : $open_count2"
echo "nmap: Nombre de ports filtrés : $filtered_count"
echo "./ft_nmap: Nombre de ports filtrés : $filtered_count2"
echo "nmap: Nombre de ports fermés : $closed_count"
echo "./ft_nmap: Nombre de ports fermés : $closed_count2"

if [[ $open_count == $open_count2 ]] && [[ $filtered_count == $filtered_count2 ]] && [[ $closed_count == $closed_count2 ]];then
	echo -e "\E[32m scan correct \E[0m"
else
	echo -e "\E[27m scan false \E[0m"
fi
